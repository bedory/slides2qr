const express = require('express');
const compression = require('compression');
const multer = require('multer');
const {v4: uuid} = require('uuid');
const fs = require('fs-extra');
const { execAsync } = require("./execAsync");
const path = require('path');
const os = require('os');
const cors = require("cors");

const tmpPath = os.tmpdir();
const publicPath = "./public/";
const uploadsRelPath = './data/uploads/';
const uploadsPath = path.join(publicPath, uploadsRelPath);
const PORT = process.env.PORT || 3001
const DOMAIN = process.env.DEV ? (!!process.env.DEV_DOMAIN ? process.env.DEV_DOMAIN : `localhost:${PORT}`) : (!!process.env.DOMAIN ? process.env.DOMAIN : "slides2qr.com");

const app = express();
const upload = multer({dest: tmpPath, limit: { fieldSize: "25MB" }});

if(process.env.DEV) {
    app.use(cors());
} else {
    app.use(cors({
        origin: ["https://slides2qr.com", "https://slides2qr.bedory.com"],
    }))
};
app.use(compression());
app.use(express.json());
app.use(express.static(publicPath));

const transformPathToUrl = (inputPath) => {
    const relativePathFromPublic = path.relative(publicPath, inputPath);
    return (process.env.DEV ? "http://" : "https://") + path.join(DOMAIN, relativePathFromPublic);
}

const getDirPath = (id) => path.resolve(uploadsPath, id);
const getFullSlidePath = (id) => path.resolve(getDirPath(id), "full.pdf");
const getGroupName = group => group.join("_");
const getGroupSlidePath = (id, group) => path.resolve(getDirPath(id), `${getGroupName(group)}.pdf`);
const getGroupQrPath = (id, group) => path.resolve(getDirPath(id), `${getGroupName(group)}.png`);
const getGroupQrZipPath = (id) => path.resolve(getDirPath(id), "QR_codes.zip");
const getSlideBaseImagePath = (id) => path.resolve(getDirPath(id), "slide.jpg");
const getSlideImagePath = (id, slideNumber) => path.resolve(getDirPath(id), `slide-${slideNumber}.jpg`);

app.get('/', (req, res) => {
    return res.status(404).end();
})

// upload slide, convert to pdf if needed and place in correct location
app.post("/api/slides", upload.single("slides"), async (req, res) => {
    process.env.VERBOSE && console.log(`Starting slides upload with slides in ${req.file.path}`);
    const id = process.env.DEV ? "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" : uuid();
    process.env.VERBOSE && fs.remove(getDirPath(id)); // if in dev mode, remove dir first to prevent bugs
    fs.ensureDirSync(getDirPath(id));
    if (req.file.mimetype === "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
        req.file.mimetype === 'application/vnd.oasis.opendocument.presentation' ||
        req.file.mimetype === 'application/vnd.ms-powerpoint') {
        process.env.VERBOSE && console.log("Starting conversion from PowerPoint to pdf.");
        await execAsync(`unoconv -d presentation -f pdf -T 600 -o "${getFullSlidePath(id)}" ${req.file.path}`);
        process.env.VERBOSE && console.log(`Successfully converted to pdf (${getFullSlidePath(id)}).`, `Removing original file (${req.file.path}).`);
        // fs.remove(req.file.path);
    } else if (req.file.mimetype === "application/pdf") {
        process.env.VERBOSE && console.log(`Moving uploaded pdf (${req.file.path}) to correct location (${getFullSlidePath(id)}).`);
        fs.move(req.file.path, getFullSlidePath(id));
    }
    res.json({id});
})

// get list of slide jpegs and generate them if they don't exist yet
app.get("/api/slides/:id", async (req, res) => {
    const id = req.params.id;
    const file = fs.readFileSync(getFullSlidePath(id), "utf-8");
    const numberSlides = file.match(/\/Type[\s]*\/Page[^s]/g).length;
    // if the image of the last slide exists, assume that they all exist
    if(!fs.existsSync(getSlideImagePath(id, numberSlides - 1)) || process.env.DEV) {
        await execAsync(`magick convert -density 150 ${getFullSlidePath(id)} -quality 50 -resize 512x512 ${getSlideBaseImagePath(id)}`);
        process.env.VERBOSE && console.log(`Successfully generated slide images with image base ${getSlideBaseImagePath(id)}.`);
    }
    const slideImageUrls = [];
    for(let i = 0; i < numberSlides; i++) {
        slideImageUrls.push(transformPathToUrl(getSlideImagePath(id, i)));
    }
    res.json({images: slideImageUrls});
})

// Generate groups
app.post("/api/slides/:id", async (req, res) => {
    const id = req.params.id;
    const groups = req.body.groups.filter(group => group.length > 0);
    process.env.VERBOSE && console.log(`Starting QR generation with id ${id}.`);

    if(!groups) {
        process.env.VERBOSE && console.log(`ERROR: groups were not specified.`);
        return res.status(400).end();
    }

    if (!fs.existsSync(getFullSlidePath(id))) {
        process.env.VERBOSE && console.log(`ERROR: Full slide doesn't exist at path ${getFullSlidePath(id)}.`);
        return res.status(404).end();
    }

    const groupQrPathsPromises = groups.map((group) => {
        execAsync(`pdftk "${getFullSlidePath(id)}" cat ${group.join(" ")} output ${getGroupSlidePath(id, group)}`);
        console.log(`pdftk "${getFullSlidePath(id)}" cat ${group.join(" ")} output ${getGroupSlidePath(id, group)}`);
        return execAsync(`qrencode -o ${getGroupQrPath(id, group)} "${transformPathToUrl(getGroupSlidePath(id, group))}"`)
                .then(() => getGroupQrPath(id, group))
    });
    Promise.all(groupQrPathsPromises).then((groupQrPaths) => {
            execAsync(`7z a ${getGroupQrZipPath(id)} ${groupQrPaths.join(" ")}`);
        }
    );
    res.json({
        zip: transformPathToUrl(getGroupQrZipPath(id)),
        groups: groups.map(group => ({
            url: transformPathToUrl(getGroupSlidePath(id, group)),
            qr: transformPathToUrl(getGroupQrPath(id, group))
        })),
    })
});

app.listen(PORT, () => {
    console.log(`Server is running on ${DOMAIN}`);
})
