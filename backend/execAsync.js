const { exec } = require('child_process');

exports.execAsync = (cmd, callback) => (
    new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            callback && callback(error, stdout, stderr);
            if( error ) {
                reject(error);
            } else {
                resolve(stdout ? stdout : stderr);
            }
        })
    })
)

