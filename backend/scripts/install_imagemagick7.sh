#!/bin/bash

apt update
apt-get install build-essential
wget https://www.imagemagick.org/download/ImageMagick.tar.gz -P /tmp/
tar xvzf /tmp/ImageMagick.tar.gz -C /tmp/
cd /tmp/ImageMagick-7.* || exit
./configure
make
make install
ldconfig /usr/local/lib
rm -rf /tmp/ImageMagick*
