import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Layout from "../components/Layout/Layout";
import {Box, Container, Paper} from "@material-ui/core";
import UploadSection from "../components/Index/UploadSection/UploadSection";
import GenerateSection from "../components/Index/GenerateSection/GenerateSection";
import DownloadSection from "../components/Index/DownloadSection/DownloadSection";
import InstructionVideo from "../components/Index/InstructionVideo/InstructionVideo";

export default function Home() {
    const [ slidesId, setSlidesId ] = useState();
    const [ slideImages, setSlideImages ] = useState();
    const [ results, setResults ] = useState();

  return (
    <Layout>
      <Head>
        <title>slides 2 QR - make presentation accessible</title>
      </Head>
        <InstructionVideo />
        <Container disableGutters={true}>
            <UploadSection setSlidesId={setSlidesId} setSlideImages={setSlideImages}
                           slideImage={slideImages && slideImages.length > 0 && slideImages[0]} setResults={setResults}/>
            {slidesId && slideImages &&
            <GenerateSection slidesId={slidesId} slideImages={slideImages} setResults={setResults}/>}
            {slidesId && slideImages && results && <DownloadSection results={results}/>}
        </Container>
    </Layout>
  )
}
