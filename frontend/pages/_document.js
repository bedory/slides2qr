import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheets } from '@material-ui/core/styles';
import theme from '../components/theme';


const description = "Slides 2 QR turns your PowerPoint slides into a QR codes that lets other people easily download them as PDF.";

export default class MyDocument extends Document {
    render() {
        return (
            <Html lang="en">
                <Head>
                    <meta charSet='utf-8'/>
                    <meta name="theme-color" content={theme.palette.primary.main}/>
                    <link
                        href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;1,200;1,300;1,400;1,600&display=swap"
                        rel="stylesheet"
                    />


                    <link rel="manifest" href="/manifest.json"/>
                    <meta name='application-name' content='slides2qr' />
                    <meta name='apple-mobile-web-app-capable' content='yes' />
                    <meta name='apple-mobile-web-app-status-bar-style' content='default' />
                    <meta name='apple-mobile-web-app-title' content='PWA App' />
                    <meta name='description' content={description}/>
                    <meta name='keywords' content='PowerPoint, slides2qr, slides, QR, QR-code, generate, transform, split, PDF'/>
                    <meta name='format-detection' content='telephone=no' />
                    <meta name='mobile-web-app-capable' content='yes' />

                    <link rel='apple-touch-icon' sizes='180x180' href='/icons/icon-180.png' />
                    <link rel='icon' type='image/png' sizes='96x96' href='/icon/favicon-96.png' />
                    <link rel='icon' type='image/png' sizes='32x32' href='/icon/favicon-32.png' />
                    <link rel='icon' type='image/png' sizes='16x16' href='/icon/favicon-16.png' />
                    <link rel='manifest' href='/manifest.json' />
                    <link rel='mask-icon' href='/icon/icon.svg' color={theme.palette.primary.main} />
                    <link rel='shortcut icon' href='/icon/favicon.ico' />

                    <meta name='twitter:card' content='summary' />
                    <meta name='twitter:url' content={process.env.NEXT_PUBLIC_DOMAIN} />
                    <meta name='twitter:title' content='slides2qr' />
                    <meta name='twitter:description' content={description} />
                    <meta name='twitter:image' content={`${process.env.NEXT_PUBLIC_DOMAIN}/icon/icon-192.png`} />
                    <meta name='twitter:creator' content='@bedory' />
                    <meta property='og:type' content='website' />
                    <meta property='og:title' content='slides2qr' />
                    <meta property='og:description' content={description} />
                    <meta property='og:site_name' content='slides2qr' />
                    <meta property='og:url' content={process.env.NEXT_PUBLIC_DOMAIN} />
                    <meta property='og:image' content={`${process.env.NEXT_PUBLIC_DOMAIN}/icon/icon-180.png`} />
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        );
    }
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with server-side generation (SSG).
MyDocument.getInitialProps = async (ctx) => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
      originalRenderPage({
        enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
      });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
  };
};
