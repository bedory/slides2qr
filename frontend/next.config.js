const withPWA = require('next-pwa')

module.exports = withPWA({
    webpack: (config, options) => {
        config.module.rules.push({
            test: /\.svg$/,
            issuer: {
                test: /\.(js|ts)x?$/,
            },
            use: ['@svgr/webpack'],
        })

        return config
    },
    pwa: {
        dest: 'public'
    }
})
