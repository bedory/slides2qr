import React, {useState} from "react";
import {
    IconButton,
    Box,
    AppBar,
    Toolbar,
    SvgIcon
} from "@material-ui/core";
import useStyles from "./Layout.styles";
import Logo from "../../public/icon/icon.svg";

function Layout({ children }) {
     const classes = useStyles();
    const [menuOpen, setMenuOpen] = useState(false);
    const toggleMenuOpen = () => setMenuOpen(prevState => !prevState);
    return (
        <Box className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.header}>
                    <IconButton edge="start" color="inherit" aria-label="slides2qr">
                        <SvgIcon component={Logo} titleAccess="slides2qr"/>
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Box className={classes.body}>
                {children}
            </Box>
        </Box>
    );
}

export default Layout;
