import { makeStyles } from '@material-ui/core/styles';

const headerHeightMobile = "6rem";
const headerHeightDesktop = "8rem";
const footerHeight = "6rem";

const useStyles = makeStyles(theme => ({
    root: {
        position: "relative",
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        backgroundImage: "url(background_bare.png), linear-gradient(to bottom right, #5eae74, #60acc9)",
        backgroundRepeat: "repeat, no-repeat",
        backgroundSize: "contain"
    },
    header: {
        justifyContent: "space-between",
    },
}))


export default useStyles;
