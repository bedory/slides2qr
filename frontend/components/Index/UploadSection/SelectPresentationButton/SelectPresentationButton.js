import React, { useState, useEffect, useRef } from 'react';
import {ButtonBase, Box, Typography, CircularProgress} from "@material-ui/core";
import useStyles from "./SelectPresentationButton.styles";

export default function SelectPresentationButton({errors, register, onChange, loading, src}) {
    const classes = useStyles();
    const button = (
        <>
            <input type="file" ref={register({required: true})} style={{display: "none"}} id="slides"
                   onChange={onChange}
                   name="slides" accept=".pdf, .pptx, .ppt, .odp"/>
            <ButtonBase className={classes.button} component="label" htmlFor="slides">
                <Typography variant="h5" className={classes.banner}>select slides</Typography>
                <img className={classes.image} src={src || "./slides_placeholder.png"} alt="slides"/>
            </ButtonBase>
        </>
    );
    return (
        <Box className={classes.root}>
            {loading ? <CircularProgress /> : button}
        </Box>
    )
}
