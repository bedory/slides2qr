import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        height: "8rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    button: {
        position: "relative",
        height: "100%",
        borderRadius: theme.shape.borderRadius,
        overflow: "hidden",
    },
    image: {
        height: "100%",
    },
    banner: {
        position: "absolute",
        backgroundColor: theme.palette.background.paper,
        opacity: 0.8,
        width: "100%",
        textAlign: "center",
        padding: theme.spacing(1),
    }
}))


export default useStyles;
