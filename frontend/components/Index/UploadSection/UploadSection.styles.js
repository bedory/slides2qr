import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: theme.spacing(1),
        margin: theme.spacing(1),
        gap: "0.5em",
        [theme.breakpoints.up("md")]: {
            flexDirection: "row",
            justifyContent: "space-evenly",
        }
    },
}))


export default useStyles;
