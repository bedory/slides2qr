import React, { useState, useEffect } from 'react';
import {Box, Paper, Button, CircularProgress} from "@material-ui/core";
import useStyles from "./UploadSection.styles";
import SelectPresentationButton from "./SelectPresentationButton/SelectPresentationButton";
import UploadPresentationButton from "./UploadPresentationButton/UploadPresentationButton";
import {useForm} from "react-hook-form";
import axios from "axios";

// TODO: use redux to have slide ID be global instead of setSlideId
// TODO: make slide_id persistent in cache
export default function UploadSection({setSlidesId, setSlideImages, slideImage, setResults}) {
    const classes = useStyles();
    const { register, handleSubmit, errors } = useForm();
    const [uploading, setUploading] = useState(false);

    const onSubmit = async (data) => {
        setUploading(true);
        setSlidesId(undefined);
        setSlideImages(undefined);
        setResults(undefined);
        let bodyFormData = new FormData();
        bodyFormData.append("slides", data.slides[0]);
        const responseSlidesUpload = await axios({
            method: 'post',
            url: `${process.env.NEXT_PUBLIC_DOMAIN}/api/slides`,
            data: bodyFormData,
            headers: {'Content-Type': 'multipart/form-data' }
        });
        setSlidesId(responseSlidesUpload.data.id);
        const responseGetSlideImages = await axios({
            method: 'get',
            url: `${process.env.NEXT_PUBLIC_DOMAIN}/api/slides/${responseSlidesUpload.data.id}`,
        });
        setSlideImages(responseGetSlideImages.data.images);
        setUploading(false);
    }

    return (
        <form>
            <Paper className={classes.root}>
                <SelectPresentationButton errors={errors.presentation} register={register}
                                          src={slideImage}
                                          onChange={handleSubmit(onSubmit)} loading={uploading}/>
            </Paper>
        </form>
    )
}
