import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./UploadPresentationButton.styles";

export default function UploadPresentationButton() {
    const classes = useStyles();
    return (
        <Button className={classes.root} type="submit" variant="contained" color="primary" size="large">
            upload
        </Button>
    )
}
