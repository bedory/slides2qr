import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "none",
        position: "absolute",
        right: 0,
        padding: theme.spacing(1),
        [theme.breakpoints.up("md")]: {
            display: "block",
        }
    },
}))


export default useStyles;
