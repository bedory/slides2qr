import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./InstructionVideo.styles";
import YouTube from 'react-youtube';

export default function InstructionVideo() {
    const classes = useStyles();
    const opts = {
        height: '108',
        width: '192',
        playerVars: {
            // https://developers.google.com/youtube/player_parameters
            autoplay: 0,
        },
    };

    const onReady = (event) =>  {
    // access to player in all event handlers via event.target
    }

    return (
        <YouTube containerClassName={classes.root} videoId="fa04BRTClZ4" opts={opts} onReady={onReady} />
    )
}
