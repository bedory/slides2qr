import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./GenerateSection.styles";
import Groups from "./Groups/Groups";
import Slides from "./Slides/Slides";
import axios from "axios";
import {useForm} from "react-hook-form";

export default function GenerateSection({slidesId, slideImages, setResults}) {
    // TODO: use redux instead of slideId to get slideId

    const classes = useStyles();
    const [groups, setGroups] = useState([[]]);
    const [indexSelectedGroup, setIndexSelectedGroup] = useState(0);
    const [generating, setGenerating] = useState(false);
    const addGroup = () => setGroups(prevGroups => {
        setIndexSelectedGroup(prevGroups.length);
        return prevGroups.concat([[]]);
    });
    const removeGroup = (indexToRemove) => setGroups(prevGroups => {
        if(indexToRemove === indexSelectedGroup) {
            setIndexSelectedGroup(0);
        }
        return prevGroups.filter((group, index) => index !== indexToRemove);
    });
    const toggleSlide = (slideNumberToToggle) => setGroups(prevGroups => {
        const copyGroups = [...prevGroups];
        const indexOfSlideNumber = copyGroups[indexSelectedGroup].findIndex(slideNumber => slideNumber === slideNumberToToggle);
        if( indexOfSlideNumber === -1) {
            copyGroups[indexSelectedGroup].push(slideNumberToToggle);
            copyGroups[indexSelectedGroup].sort();
        } else {
            copyGroups[indexSelectedGroup].splice(indexOfSlideNumber, 1);
        }
        return copyGroups;
    })
    const selectAllSlides = () => setGroups(prevGroups => {
        const newGroup = [];
        for(let i = 1; i<=slideImages.length; i++) {
            newGroup.push(i);
        }
        const copyGroups = [...prevGroups];
        copyGroups[indexSelectedGroup] = newGroup;
        return copyGroups;
    });
    const deselectAllSlides = () => setGroups(prevGroups => {
        const newGroup = [];
        const copyGroups = [...prevGroups];
        copyGroups[indexSelectedGroup] = newGroup;
        return copyGroups;
    });
    const { register, handleSubmit, errors } = useForm();
    const generateQrCodes = async (data) => {
        if (!slidesId) {
            console.log("first upload slide");
        }
        setGenerating(true);
        setResults(undefined);
        const response = await axios({
            method: "post",
            url: `${process.env.NEXT_PUBLIC_DOMAIN}/api/slides/${slidesId}`,
            data: {
                groups
            },

        })
        setResults(response.data);
        setGenerating(false);
    }

    return (
        <Box className={classes.root}>
            <Slides slideImages={slideImages} group={groups[indexSelectedGroup]} toggleSlide={toggleSlide}
                    selectAllSlides={selectAllSlides} deselectAllSlides={deselectAllSlides}/>
            <Groups groups={groups} indexSelectedGroup={indexSelectedGroup} addGroup={addGroup}
                    setIndexSelectedGroup={setIndexSelectedGroup} removeGroup={removeGroup}
                    generate={handleSubmit(generateQrCodes)} generating={generating}/>
        </Box>
    )
}
