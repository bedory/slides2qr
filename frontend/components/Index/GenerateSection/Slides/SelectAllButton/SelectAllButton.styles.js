import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        height: "2.5rem",
        width: "10rem",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.success.dark,
        color: theme.palette.background.default,
    },
}))


export default useStyles;
