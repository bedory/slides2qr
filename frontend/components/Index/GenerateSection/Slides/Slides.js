import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./Slides.styles";
import Slide from "./Slide/Slide";
import SelectAllButton from "./SelectAllButton/SelectAllButton";
import DeselectAllButton from "./DeselectAllButton/DeselectAllButton";

export default function Slides({slideImages, toggleSlide, group, selectAllSlides, deselectAllSlides}) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Box className={classes.slides}>
                {slideImages.map((image, index) => <Slide src={image} slideNumber={index + 1} key={image}
                                                          selected={group && group.includes(index + 1)}
                                                          onClick={() => toggleSlide(index + 1)}/>)}
            </Box>
            <Box className={classes.buttons}>
                <DeselectAllButton onClick={deselectAllSlides} />
                <SelectAllButton onClick={selectAllSlides} />
            </Box>
        </Paper>
    )
}
