import React, { useState, useEffect } from 'react';
import {ButtonBase, Box, Typography} from "@material-ui/core";
import useStyles from "./Slide.styles";

export default function Slide({src, slideNumber, onClick, selected}) {
    const classes = useStyles({selected});
    return (
        <ButtonBase className={classes.root} onClick={onClick}>
            <img className={classes.image} src={src}  alt={`slide number ${slideNumber}`}/>
        </ButtonBase>
    )
}
