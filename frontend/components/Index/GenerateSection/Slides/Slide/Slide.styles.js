import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: ({selected}) => ({
        position: "relative",
        height: "100%",
        borderRadius: theme.shape.borderRadius,
        overflow: "hidden",
        opacity: selected ? 1 : 0.5,
    }),
    image: {
        width: "100%",
    },
}))


export default useStyles;
