import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(1),
        borderRadius: `${theme.shape.borderRadius} ${theme.shape.borderRadius} 0 0`,
        [theme.breakpoints.up("md")]: {
            order: "2",
            borderRadius: `0 ${theme.shape.borderRadius} ${theme.shape.borderRadius} 0`,
            width: "100%",
        }
    },
    slides: {
        display: "grid",
        gridTemplateColumns: "repeat(auto-fit, minmax(12em, 1fr))",
        gridGap: "0.5em",
        height: "min-content",
        marginBottom: theme.spacing(1),
    },
    buttons: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        gap: "0.5em",
    }
}))


export default useStyles;
