import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./DeselectAllButton.styles";

export default function Slide({onClick}) {
    const classes = useStyles();
    return (
        <Button className={classes.root} onClick={onClick} variant="contained">
            Deselect All
        </Button>
    )
}
