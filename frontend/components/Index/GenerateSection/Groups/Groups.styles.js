import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: theme.spacing(1),
        gap: "0.5em",
        borderRadius: `0 0 ${theme.shape.borderRadius} ${theme.shape.borderRadius}`,
        [theme.breakpoints.up("md")]: {
            order: "1",
            borderRadius: `${theme.shape.borderRadius} 0 0 ${theme.shape.borderRadius}`,
        }
    },
}))


export default useStyles;
