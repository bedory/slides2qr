import React, { useState, useEffect } from 'react';
import {IconButton, Typography} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import useStyles from "./DeleteGroupButton.styles";

export default function SelectGroupButton({disabled, onClick}) {
    const classes = useStyles();
    return (
        <IconButton className={classes.root} disabled={disabled} onClick={onClick}>
            <DeleteIcon />
        </IconButton>
    )
}
