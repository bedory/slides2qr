import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        borderRadius: `0 ${theme.shape.borderRadius} ${theme.shape.borderRadius} 0`,
        backgroundColor: theme.palette.error.dark,
        color: theme.palette.background.default,
    },
}))


export default useStyles;
