import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./Group.styles";
import SelectGroupButton from "./SelectGroupButton/SelectGroupButton";
import DeleteGroupButton from "./DeleteGroupButton/DeleteGroupButton";

export default function Group({group, groupNumber, selected, removable, select, remove}) {
    const classes = useStyles();
    return (
        <Box className={classes.root}>
            <SelectGroupButton group={group} groupNumber={groupNumber} selected={selected} onClick={select}/>
            <DeleteGroupButton disabled={!removable} onClick={remove}/>
        </Box>
    )
}
