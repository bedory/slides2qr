import React, { useState, useEffect } from 'react';
import {ButtonBase, Typography} from "@material-ui/core";
import useStyles from "./SelectGroupButton.styles";

export default function SelectGroupButton({group, groupNumber, selected, onClick}) {
    const empty = !(group.length > 0)
    const classes = useStyles({selected, empty});
    return (
        <ButtonBase className={classes.root} onClick={onClick}>
            <Typography color="textSecondary" variant="h6">Group {groupNumber + 1}</Typography>
            <Typography color="textSecondary" className={classes.slideNumbersText}>
                { empty ? "empty" : group.slice(0, 3).join(", ")}
                { group.length > 3 && ", ..."}
            </Typography>
        </ButtonBase>
    )
}
