import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: ({selected}) => ({
        display: "flex",
        flexDirection: "column",
        borderRadius: `${theme.shape.borderRadius} 0 0 ${theme.shape.borderRadius}`,
        backgroundColor: theme.palette.secondary.main,
        padding: theme.spacing(1, 3),
        width: "10em",
        border: selected ? `5px solid ${theme.palette.primary.main}` : "none",
    }),
    slideNumbersText: ({empty}) => ({
        fontStyle: empty ? "italic" : "normal",
    })
}))


export default useStyles;
