import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        gap: "0.25em",
        height: "5rem",
    },
}))


export default useStyles;
