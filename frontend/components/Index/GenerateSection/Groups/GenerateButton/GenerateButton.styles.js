import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        height: "3rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    button: {
        height: "100%",
        borderRadius: theme.shape.borderRadius,
    },
}))


export default useStyles;
