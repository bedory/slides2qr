import React, { useState, useEffect } from 'react';
import {Box, Button, CircularProgress} from "@material-ui/core";
import useStyles from "./GenerateButton.styles";

export default function UploadPresentationButton({onClick, loading}) {
    const classes = useStyles();
    const button = (
        <Button className={classes.button} color="primary" variant="contained" size="large" onClick={onClick}>
            generate
        </Button>
    )
    return (
        <Box className={classes.root}>
            {loading ? <CircularProgress/> : button}
        </Box>
    )
}
