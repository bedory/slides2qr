import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./Groups.styles";
import Group from "./Group/Group";
import AddGroupButton from "./AddGroupButton/AddGroupButton";
import GenerateButton from "./GenerateButton/GenerateButton";

export default function Groups({groups, indexSelectedGroup, addGroup, setIndexSelectedGroup, removeGroup, generate, generating}) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            {groups.map((group, index) => <Group key={index} group={group} groupNumber={index}
                                                 selected={indexSelectedGroup === index}
                                                 select={() => setIndexSelectedGroup(index)}
                                                 remove={() => removeGroup(index)}
                                                 removable={groups.length > 1}/>)}
            <AddGroupButton onClick={addGroup}/>
            <GenerateButton onClick={generate} loading={generating} />
        </Paper>
    )
}
