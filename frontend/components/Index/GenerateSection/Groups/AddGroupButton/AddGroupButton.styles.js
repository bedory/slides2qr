import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.success.dark,
        color: theme.palette.background.default,
    },
    icon: {
        color: theme.palette.background.default,
    }
}))


export default useStyles;
