import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./AddGroupButton.styles";
import AddRoundedIcon from '@material-ui/icons/AddRounded';

export default function UploadPresentationButton({onClick}) {
    const classes = useStyles();
    return (
        <Button className={classes.root} variant="contained" size="medium" onClick={onClick}
                endIcon={<AddRoundedIcon className={classes.icon}/>}>
            add group
        </Button>
    )
}
