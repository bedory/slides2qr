import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        margin: theme.spacing(1),
        gap: "0.5em",
        [theme.breakpoints.up("md")]: {
            flexDirection: "row",
        }
    },
}))


export default useStyles;
