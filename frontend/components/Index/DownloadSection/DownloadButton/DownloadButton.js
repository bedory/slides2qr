import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./DownloadButton.styles";

export default function UploadPresentationButton({zipUrl}) {
    const classes = useStyles();
    return (
        <Button component="a" className={classes.root} color="primary" variant="contained" size="large" href={zipUrl}>
            Download
        </Button>
    )
}
