import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./DownloadSection.styles";
import Groups from "./Groups/Groups";
import DownloadButton from "./DownloadButton/DownloadButton";

export default function DownloadSection({results}) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Groups groups={results.groups}/>
            <DownloadButton zipUrl={results.zip}/>
        </Paper>
    )
}
