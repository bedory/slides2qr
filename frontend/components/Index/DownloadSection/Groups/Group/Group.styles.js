import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: theme.spacing(1),
        gap: "0.5em",
        width: "min-content"
    },
    image: {
        width: "10rem",
    }
}))


export default useStyles;
