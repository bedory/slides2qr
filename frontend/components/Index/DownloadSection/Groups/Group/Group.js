import React, { useState, useEffect } from 'react';
import {Box, Paper, Button, Typography} from "@material-ui/core";
import useStyles from "./Group.styles";
import CopyQrButton from "./CopyQrButton/CopyQrButton";
import OpenLinkButton from "./OpenLinkButton/OpenLinkButton";

export default function Group({group, groupNumber}) {
    const classes = useStyles();
    const askWritePermission = async () => {
        try {
            const { state } = await navigator.permissions.query({name: "clipboard-write"});
            return state === "granted";
        } catch (e) {
            return false;
        }
    }
    const copyToClipboard = async (textContentOrImageUrl, type) => {
        if(await askWritePermission()) {
            if(type === "text") {
                await navigator.clipboard.writeText(textContentOrImageUrl);
            } else if (type === "image") {
                const response = await fetch(textContentOrImageUrl);
                const blob = await response.blob();
                await navigator.clipboard.write([new ClipboardItem({ [blob.type]: blob })]);
            }
        } else {
            console.log("No write to clipboard permissions.");
        }
    }
    return (
        <Paper className={classes.root}>
           <Typography variant="h5">Group {groupNumber + 1}</Typography>
            <img className={classes.image} src={group.qr}  alt="qr code for "/>
            <CopyQrButton onClick={() => copyToClipboard(group.qr, "image")}/>
            <OpenLinkButton href={group.url}/>
        </Paper>
    )
}
