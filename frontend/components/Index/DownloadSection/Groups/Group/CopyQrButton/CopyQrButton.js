import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./CopyQrButton.styles";

export default function UploadPresentationButton({onClick}) {
    const classes = useStyles();
    return (
        <Button className={classes.root} variant="contained" onClick={onClick}>
            copy QR
        </Button>
    )
}
