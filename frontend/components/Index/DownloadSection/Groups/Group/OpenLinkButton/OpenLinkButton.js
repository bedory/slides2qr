import React, { useState, useEffect } from 'react';
import {Button} from "@material-ui/core";
import useStyles from "./OpenLinkButton.styles";

export default function UploadPresentationButton({href}) {
    const classes = useStyles();
    return (
        <Button component="a" className={classes.root} variant="contained" href={href} target="_blank"
                rel="noopener noreferrer">
            open link
        </Button>
    )
}
