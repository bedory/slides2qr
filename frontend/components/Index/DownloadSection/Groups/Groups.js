import React, { useState, useEffect } from 'react';
import {Box, Paper, Button} from "@material-ui/core";
import useStyles from "./Groups.styles";
import Group from "./Group/Group";

export default function Groups({groups}) {
    const classes = useStyles();
    return (
        <Box className={classes.root}>
            {groups.map((group, index) => <Group group={group} key={group.url} groupNumber={index} />)}
        </Box>
    )
}
