import {createMuiTheme, responsiveFontSizes} from "@material-ui/core/styles";

let theme = createMuiTheme({
    palette: {
        primary: {
            main: "rgb(67, 159, 193)",
        },
        secondary: {
            light: "rgb(100, 100, 100)",
            main: "rgb(80, 80, 80)",
            dark: "rgb(60, 60, 60)",
        },
        text: {
            secondary: "rgba(255, 255, 255, 0.95)",
        }
    },
    typography: {
        fontFamily: "'Nunito', sans-serif",
        fontWeightLight: 200,
        fontWeightRegular: 300,
        fontWeightMedium: 400,
        fontWeightBold: 600
    },
    shape: {
        borderRadius: "1rem",
    }
});
theme = responsiveFontSizes(theme);

export default theme;
